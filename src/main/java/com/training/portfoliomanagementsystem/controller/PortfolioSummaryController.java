package com.training.portfoliomanagementsystem.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.training.portfoliomanagementsystem.dto.PortfolioResponseDto;
import com.training.portfoliomanagementsystem.service.PortfolioSummaryService;

import lombok.RequiredArgsConstructor;

@RestController
@RequestMapping("/api/v1")
@RequiredArgsConstructor
public class PortfolioSummaryController {
	
	private final PortfolioSummaryService portfolioSummaryService;
	
	

}
