package com.training.portfoliomanagementsystem.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.training.portfoliomanagementsystem.entity.Position;

public interface PositionRepository extends JpaRepository<Position, String> {

}
