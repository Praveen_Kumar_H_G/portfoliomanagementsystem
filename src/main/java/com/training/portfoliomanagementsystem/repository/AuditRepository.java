package com.training.portfoliomanagementsystem.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.training.portfoliomanagementsystem.entity.Audit;

public interface AuditRepository extends JpaRepository<Audit, Long> {

}
