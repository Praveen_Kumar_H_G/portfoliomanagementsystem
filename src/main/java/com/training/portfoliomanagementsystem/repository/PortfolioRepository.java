package com.training.portfoliomanagementsystem.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.training.portfoliomanagementsystem.entity.Portfolio;

public interface PortfolioRepository extends JpaRepository<Portfolio, String> {

}
