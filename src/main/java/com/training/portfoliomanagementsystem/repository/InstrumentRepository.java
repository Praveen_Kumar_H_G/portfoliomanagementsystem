package com.training.portfoliomanagementsystem.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.training.portfoliomanagementsystem.entity.Instrument;

public interface InstrumentRepository extends JpaRepository<Instrument, String>{

}
