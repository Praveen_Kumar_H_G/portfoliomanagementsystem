package com.training.portfoliomanagementsystem.entity;

import java.time.LocalDate;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
@Builder
@AllArgsConstructor
@Data
@NoArgsConstructor
@Entity
@Table(name ="audits")
public class Audit {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long auditId;
	@ManyToOne
	private Position transactionRef;
	private LocalDate auditDate;


}
