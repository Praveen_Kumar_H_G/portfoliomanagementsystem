package com.training.portfoliomanagementsystem.entity;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@AllArgsConstructor
@Data
@NoArgsConstructor
@Entity
@Table(name ="instruments")
public class Instrument {
	@Id
	private String instrumentId;
	private String instrumentName;
	private InstrumentType instrumentType;


}
