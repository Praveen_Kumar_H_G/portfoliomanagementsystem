package com.training.portfoliomanagementsystem.entity;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@AllArgsConstructor
@Data
@NoArgsConstructor
@Entity
@Table(name ="portfolios")
public class Portfolio {
	@Id
	private String customerId;
	private String customerName;
	private String portfolioNumber;
	private double portfolioValue;
	private double currentPerformance;
	private InvestMentStratergy stratergy;

}
