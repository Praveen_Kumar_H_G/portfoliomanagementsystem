package com.training.portfoliomanagementsystem.entity;

import java.time.LocalDate;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@AllArgsConstructor
@Data
@NoArgsConstructor
@Entity
@Table(name ="positions")
public class Position {
	@Id
	private String TransactionRef;
	@ManyToOne
	private Instrument instrumentId;
	private TradeType tradeType;
	private int units;
	private double tradeValue;
	@ManyToOne
	private Portfolio customerId;
	private LocalDate date;


}
