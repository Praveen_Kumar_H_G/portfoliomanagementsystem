package com.training.portfoliomanagementsystem.exception;

import java.util.HashMap;
import java.util.Map;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class GlobalExceptionHandler {

	@ExceptionHandler(MethodArgumentNotValidException.class)
	public ResponseEntity<ValidationDto> handllerMethodAurgumentException(MethodArgumentNotValidException ex) {
		Map<String, String> errorMap = new HashMap<>();
		ex.getBindingResult().getFieldErrors()
				.forEach(error -> errorMap.put(error.getField(), error.getDefaultMessage()));
		ValidationDto validationMessage = new ValidationDto();
		validationMessage.setValidationErrors(errorMap);
		validationMessage.setErrorCode("FSA0003");
		validationMessage.setErrorMessage("please add valid input data");
		return new ResponseEntity<>(validationMessage, HttpStatus.BAD_REQUEST);
	}

	@ExceptionHandler(HttpMessageNotReadableException.class)
	public ResponseEntity<ErrorResponse> messageNotReadableHandler(HttpMessageNotReadableException ex) {
		ErrorResponse error = new ErrorResponse("FSA0004", ex.getMessage());
		return new ResponseEntity<>(error, HttpStatus.BAD_REQUEST);
	}

	

	@ExceptionHandler(AuditNotFoundException.class)
	public ResponseEntity<ErrorResponse> auditNotFoundExceptionHandler(AuditNotFoundException ex) {
		ErrorResponse error = new ErrorResponse("EX0003", ex.getMessage());
		return new ResponseEntity<>(error, HttpStatus.BAD_REQUEST);
	}
	
	@ExceptionHandler(CustomerNotFoundException.class)
	public ResponseEntity<ErrorResponse> customerNotFoundExceptionHandler(CustomerNotFoundException ex) {
		ErrorResponse error = new ErrorResponse("EX0002", ex.getMessage());
		return new ResponseEntity<>(error, HttpStatus.BAD_REQUEST);
	}
	
	@ExceptionHandler(PortfolioNotFoundException.class)
	public ResponseEntity<ErrorResponse> portfolioNotFoundExceptionHandler(PortfolioNotFoundException ex) {
		ErrorResponse error = new ErrorResponse("EX0002", ex.getMessage());
		return new ResponseEntity<>(error, HttpStatus.BAD_REQUEST);
	}
	
}
