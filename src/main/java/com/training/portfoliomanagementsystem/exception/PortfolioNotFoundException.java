package com.training.portfoliomanagementsystem.exception;

public class PortfolioNotFoundException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public PortfolioNotFoundException(String message) {
		super(message);
	}

}
