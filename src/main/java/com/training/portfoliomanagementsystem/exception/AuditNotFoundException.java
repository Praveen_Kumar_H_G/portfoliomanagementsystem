package com.training.portfoliomanagementsystem.exception;

public class AuditNotFoundException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public AuditNotFoundException(String message) {
		super(message);
	}

}
