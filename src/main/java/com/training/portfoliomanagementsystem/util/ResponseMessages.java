package com.training.portfoliomanagementsystem.util;

public interface ResponseMessages {

	/**
	 * Success Responses
	 */
	String COURSE_ENROLLED_SUCCESSFULLY_CODE = "SUCCESS001";
	String COURSE_ENROLLED_SUCCESSFULLY_MESSAGE = "Course Enrolled Successfully";
	String COURSE_RESCHEDULED_SUCCESSFULLY_CODE = "SUCCESS002";
	String COURSE_RESCHEDULED_SUCCESSFULLY_MESSAGE = "Course Rescheduled Successfully";
	String COURSE_CANCELLED_SUCCESSFULLY_CODE = "SUCCESS003";
	String COURSE_CANCELLED_SUCCESSFULLY_MESSAGE = "Course Cancelled Successfully";
	String ENROLLED_DETAILS_FETCHED_SUCCESSFULLY_CODE = "SUCCESS004";
	String ENROLLED_DETAILS_FETCHED_SUCCESSFULLY_MESSAGE = "Available Course Enrolled details fetched Successfully";
	
	String COURSE_DETAILS_SUCCESSFULLY_FETCHED_MESSAGE = "Course are successfully fetched";
	String COURSE_DETAILS_SUCCESSFULLY_FETCHED_CODE= "SUCCESS001";
	
	/**
	 * Exception Responses
	 */
	
	String DATE_MISS_MATCH_EXCEPTION_CODE = "EX0001";
	String DATE_MISS_MATCH_EXCEPTION_MESSAGE = "DateMissmatchException";
	
	String COURSE_NOT_FOUND_EXCEPTION_CODE = "EX0002";
	String COURSE_NOT_FOUND_EXCEPTION_MESSAGE = "Course details not found";
}